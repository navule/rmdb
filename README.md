# README #

1. Install node.js and gulp globally in your machine and add both to the system environment path
2. Open command prompt
3. Change Directory to the root of this project's location and execute the following commands
        > npm install
        > gulp
4. Now open index.html file generated in dist folder and see the app running

### What is this repository for? ###

* ReactJS based Movie Database RMDB
* 1.0.0
* [Learn Markdown](https://bitbucket.org/navule/rmdb/overview)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies : ReactJs, NodeJs, Flux Architecture
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* @navule